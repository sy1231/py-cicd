FROM python:3.5.1
COPY /. /app
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
ENV PORT 8000
CMD python app.py $PORT
EXPOSE $PORT/tcp
